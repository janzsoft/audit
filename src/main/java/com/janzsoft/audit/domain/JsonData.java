package com.janzsoft.audit.domain;


import com.janzsoft.audit.exception.BaseException;
import com.janzsoft.audit.exception.ResultObj;

import java.util.LinkedHashMap;

/**
 * 返回给前段异步接口的json形式
 */
public class JsonData extends LinkedHashMap<String, Object> {

    private static final String KEY_RESULT = "result";

    public JsonData() {
        this(true);
    }

    public JsonData(boolean hasResultObj) {
        super();
        if (hasResultObj) {
            this.put(KEY_RESULT, ResultObj.getSuccResult());
        }
    }


    public void setErrResult(int resCode, String errMsg) {
        ResultObj resultObj = (ResultObj) this.get(KEY_RESULT);

        if (resultObj == null) {
            resultObj = ResultObj.getFailResult(resCode, errMsg);
            this.put(KEY_RESULT, resultObj);
        }

        resultObj.setResCode(resCode);
        resultObj.setErrMsg(errMsg);
    }

    public void setErrResult(BaseException e) {
        int code = e.getCode();
        String errMsg = e.getMessage();
        setErrResult(code, errMsg);
    }
}
