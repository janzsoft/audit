package com.janzsoft.audit.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.janzsoft.audit.constant.ResultCodeConstants;
import com.janzsoft.audit.exception.BaseException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 分析json格式信息的方法
 * read将json信息转换成对象
 * write 将对象转换成string类型的json
 */
public class JsonParser {

    private static Logger logger = Logger.getLogger("JsonParser");

    /**
     * ObjectMapper线程安全，可以作为单例使用
     */
    private static ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 读取父节点下指定子节点
     *
     * @param node     父节点
     * @param pathName 子节点key链
     * @return
     */
    public static JsonNode read(JsonNode node, String... pathName) {

        JsonNode child = node;
        try {
            for (String path : pathName) {
                child = child.path(path);
            }
        } catch (Exception e) {
            logger.error("读取子节点失败:" + Arrays.toString(pathName), e);
            return null;
        }
        return child;
    }

    /**
     * 读取父节点下指定子节点
     *
     * @param node     父节点
     * @param pathName 子节点key链
     * @return
     */
    public static JsonNode read(String node, String... pathName) {

        if (node == null) {
            return null;
        }

        try {
            return read(objectMapper.readTree(node), pathName);
        } catch (Exception e) {
            logger.error("读取子节点失败:" + Arrays.toString(pathName), e);
            return null;
        }

    }

    /**
     * 读取父节点下指定子节点，并将其解析为指定类型对象
     *
     * @param node     父节点
     * @param type     类型
     * @param pathName 子节点key链
     * @param <T>      范型
     * @return
     */
    public static <T> T read(JsonNode node, Class<T> type, String... pathName) {

        JsonNode child = read(node, pathName);
        try {
            return objectMapper.readValue(child.toString(), type);
        } catch (IOException e) {
            logger.error("节点类型转换失败:node=" + child + ", type=" + type, e);
            return null;
        }
    }

    /**
     * 将json数据解析为指定类型对象
     * 默认只解析当前节点，不会将子节点递归解析为对象
     *
     * @param json json数据
     * @param type 类型
     * @param <T>  范型
     * @return
     */
    public static <T> T read(String json, Class<T> type, String... pathName) {
        JsonNode child = read(json, pathName);
        try {
            if (StringUtils.isNotBlank(child.toString())){
                return objectMapper.readValue(child.toString(), type);
            }else {
                return null;
            }
        } catch (IOException e) {
            logger.error("节点类型转换失败:node=" + child + ", type=" + type, e);
            return null;
        }
    }

    /**
     * 将节点解析为指定类型对象列表
     *
     * @param node 节点
     * @param type 类型
     * @param <T>  范型
     * @return
     */
    public static <T> List<T> readList(JsonNode node, Class<T> type, String... pathName) {

        node = read(node, pathName);

        if (!node.isArray()) {
            return null;
        }

        List<JsonNode> nodeList = new ArrayList<JsonNode>();
        Iterator<JsonNode> it = node.iterator();
        while (it.hasNext()) {
            nodeList.add(it.next());
        }

        List<T> valueList = new ArrayList<T>();
        for (JsonNode child : nodeList) {
            valueList.add(read(child, type));
        }

        return valueList;
    }

    /**
     * 将节点解析为指定类型对象列表
     *
     * @param node 节点
     * @param type 类型
     * @param <T>  范型
     * @return
     */
    public static <T> List<T> readList(String node, Class<T> type, String... pathName) {

        try {
            return readList(objectMapper.readTree(node), type, pathName);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 将输入流解析为json结点
     *
     * @param in 输入流
     * @return
     */
    public static JsonNode readTree(InputStream in) {
        JsonNode result = null;
        try {
            result = objectMapper.readTree(in);
        } catch (Exception e) {
            logger.error("解析stream至json结点失败", e);
        }
        return result;
    }

    /**
     * 将对象解析为json数据
     *
     * @param obj 对象
     * @return
     */
    public static String write(Object obj) {
        String result = null;
        try {
            result = objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("解析对象至json数据失败", e);
        }
        return result;
    }

    /**
     * 检验服务器端响应是否成功
     *
     * @param rootNode 响应
     */
    public static void checkResult(JsonNode rootNode) throws BaseException {
        JsonNode resultNode = rootNode.path("result");
        if (resultNode != null) {
            int resCode = resultNode.path("resCode").asInt();
            String errMsg = resultNode.path("errMsg").asText();

            if (resCode != ResultCodeConstants.SUCCESS) {
                throw new BaseException(resCode, errMsg);
            }
        }
    }

    /**
     * 检验服务器端响应是否成功
     *
     * @param rootNode 响应
     */
    public static void checkResult(String rootNode) throws BaseException {
        try {
            checkResult(objectMapper.readTree(rootNode));
        } catch (BaseException e) {
            throw e;
        } catch (IOException ioe) {
            throw new BaseException(500, "数据格式错误" + rootNode);
        }
    }

}
