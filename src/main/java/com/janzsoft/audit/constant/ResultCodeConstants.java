package com.janzsoft.audit.constant;

/**
 * 错误码的enums。依据不同的逻辑设置不同的错误码
 *
 * Created by gaoqi on 2017/8/25.
 */
public class ResultCodeConstants {

    /**
     * 成功
     */
    public static final int SUCCESS = 200;

}
