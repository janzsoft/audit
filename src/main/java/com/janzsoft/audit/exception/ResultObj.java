package com.janzsoft.audit.exception;


import com.janzsoft.audit.constant.ResultCodeConstants;

/**
 * 错误的信息对象
 */
public class ResultObj {

    private int resCode;//错误码

    private String errMsg;//错误的信息

    public ResultObj() {

    }

    public ResultObj(int resCode, String errMsg) {
        this.resCode = resCode;
        this.errMsg = errMsg;
    }

    public static ResultObj getSuccResult() {
        return new ResultObj(ResultCodeConstants.SUCCESS, "成功");
    }

    public static ResultObj getFailResult(int resCode, String errMsg) {
        return new ResultObj(resCode, errMsg);
    }

    public int getResCode() {
        return resCode;
    }

    public void setResCode(int resCode) {
        this.resCode = resCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
