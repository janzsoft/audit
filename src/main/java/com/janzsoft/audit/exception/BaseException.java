package com.janzsoft.audit.exception;

/**
 * 自定义异常，返回给前段展示
 * eg 500,"登录名错误"
 */
public class BaseException extends RuntimeException {

    private static final long serialVersionUID = 8002283590289218255L;

    private String msg;
    private int code;

    protected BaseException(int code, String arg0, Throwable arg1) {
        super(arg0, arg1);
        this.code = code;
        this.msg = arg0;
    }


    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public BaseException(ResultObj resultObj) {
        super(resultObj.getErrMsg());
        this.code = resultObj.getResCode();
        this.msg = resultObj.getErrMsg();
    }

    @Override
    public String getMessage() {
        return this.msg;
    }

    public int getCode() {
        return this.code;
    }


//    public static void main(String[] args) {
//
//        BaseException btc = new BaseException(ResultCodeConstants.TRADE_NOT_BEGIN, MessageContainer.getMsg(ResultCodeConstants.TRADE_NOT_BEGIN, "BTC"));
//        System.out.println(btc);
//
//    }
}

